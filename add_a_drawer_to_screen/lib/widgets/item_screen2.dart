import 'package:flutter/material.dart';

class ItemScreen2 extends StatelessWidget {
  const ItemScreen2({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text('Item 2'),
    );
  }
}